# --- PYODIDE:env --- #

import matplotlib         # Indispensable (provoque la déclaration de PyodidePlot)
plt = PyodidePlot()  # Disponible après l'import de matplotlib

# --- PYODIDE:code --- #

xs = [-3 + k * 0.1 for k in range(61)]
ys = [x**2 for x in xs]
plt.plot(xs, ys, "r-")
plt.grid()  # Optionnel : pour voir le quadrillage
plt.axhline()  # Optionnel : pour voir l'axe des abscisses
plt.axvline()  # Optionnel : pour voir l'axe des ordonnées
plt.title("La fonction carré")
plt.show()
